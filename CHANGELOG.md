## [0.12.0] - 2019-01-02

### Removed
- Removed `PartialEq` and `Eq` implementations for `TableIter` struct

### Changed
- Bumped `tarrasque` version to `0.10.0`

## [0.11.0] - 2018-12-29

### Changed
- Changed type of `Record::tag` to `Tag`
- Renamed `Sfnt::iter` to `Sfnt::tables`
- Renamed `SfntIter` to `TableIter`

### Added
- Added `Tag` struct
- Added implementations of `Span` for SFNT types

## [0.10.0] - 2018-12-27

### Changed
- Bumped `tarrasque` version to `0.9.0`

## [0.9.0] - 2018-12-26

### Changed
- Changed type of `Record::tag` to `&[u8]`
- Bumped `tarrasque` version to `0.8.0`

## [0.8.1] - 2018-12-11

### Changed
- Bumped `tarrasque` version to `0.7.0`

## [0.8.0] - 2018-12-08

### Changed
- Bumped `tarrasque` version to `0.6.0`
- Upgraded to Rust 2018

## [0.7.0] - 2018-06-17

### Changed
- Bumped `tarrasque` version to `0.5.0`

## [0.6.0] - 2018-06-03

### Changed
- Moved `parse_sfnt` to `Sfnt::parse`

### Fixed
- Fixed `Sfnt::find`

## [0.5.0] - 2018-05-07

### Changed
- Bumped `tarrasque` version to `0.4.0`
- Made `Sfnt` fields public

## [0.4.0] - 2018-04-30

### Added
- Added `Into<f32>` implementation for `Fixed16_16`

### Changed
- Improved performance

## [0.3.0] - 2018-04-29

### Changed
- Bumped `tarrasque` version to `0.3.0`

## [0.2.0] - 2018-04-27

### Added
- Added `DoubleEndedIterator` and `ExactSizeIterator` implementations for `SfntIter`

### Changed
- Bumped `tarrasque` version to `0.2.0`

## [0.1.0] - 2018-04-25
- Initial release
