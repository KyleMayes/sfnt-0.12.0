// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate sfnt;

use std::fs::{File};
use std::io::{Read};

use sfnt::{Record, Sfnt, Tag};

fn main() {
    // Read the font file into memory.
    let mut file = File::open("tests/resources/OpenSans-Italic.ttf").unwrap();
    let mut bytes = vec![];
    file.read_to_end(&mut bytes).unwrap();

    // Parse the font file and find one of the tables in the font file.
    let sfnt = Sfnt::parse(&bytes).unwrap();
    let (record, bytes) = sfnt.find(b"head").unwrap();

    assert_eq!(record, Record {
        tag: Tag(b"head"),
        checksum: 4165466467,
        offset: 316,
        length: 54,
    });

    assert_eq!(bytes.len(), 54);
}
