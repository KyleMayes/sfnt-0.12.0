// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate microbench;
extern crate sfnt;

use std::fs::{File};
use std::io::{Read};
use std::path::{Path};

use microbench::{Options};

use sfnt::{Sfnt, checksum};

fn with_bytes<P: AsRef<Path>, F: Fn(&[u8])>(path: P, f: F) {
    let mut file = File::open(path).unwrap();
    let mut bytes = vec![];
    file.read_to_end(&mut bytes).unwrap();
    f(&bytes);
}

fn bench_parse_sfnt() {
    with_bytes("tests/resources/OpenSans-Italic.ttf", |bytes| {
        let options = Options::default();
        microbench::bench(&options, "parse_OpenSans-Italic", || Sfnt::parse(bytes).unwrap());
    });
}

fn bench_find() {
    with_bytes("tests/resources/OpenSans-Italic.ttf", |bytes| {
        let sfnt = Sfnt::parse(bytes).unwrap();
        let options = Options::default();
        microbench::bench(&options, "find_DSIG_OpenSans-Italic", || sfnt.find(b"DSIG").unwrap());
        microbench::bench(&options, "find_glyf_OpenSans-Italic", || sfnt.find(b"glyf").unwrap());
        microbench::bench(&options, "find_prep_OpenSans-Italic", || sfnt.find(b"prep").unwrap());
    });
}

fn bench_checksum() {
    with_bytes("tests/resources/OpenSans-Italic.ttf", |bytes| {
        let (_, bytes) = Sfnt::parse(bytes).unwrap().find(b"kern").unwrap();
        let options = Options::default();
        microbench::bench(&options, "checksum_kern_OpenSans-Italic", || checksum(bytes));
    });
}

fn main() {
    bench_parse_sfnt();
    bench_find();
    bench_checksum();
}
