# sfnt

[![crates.io](https://img.shields.io/crates/v/sfnt.svg)](https://crates.io/crates/sfnt)
[![docs.rs](https://docs.rs/sfnt/badge.svg)](https://docs.rs/sfnt)
[![travis-ci.com](https://travis-ci.org/glyph-rs/sfnt.svg?branch=master)](https://travis-ci.org/glyph-rs/sfnt)

A zero-allocation SFNT parser.

Released under the Apache License 2.0.

Supported on Rust 1.31.0 and later.

## Example

```rust
use std::fs::{File};
use std::io::{Read};

use sfnt::{Record, Sfnt, Tag};

fn main() {
    // Read the font file into memory.
    let mut file = File::open("tests/resources/OpenSans-Italic.ttf").unwrap();
    let mut bytes = vec![];
    file.read_to_end(&mut bytes).unwrap();

    // Parse the font file and find one of the tables in the font file.
    let sfnt = Sfnt::parse(&bytes).unwrap();
    let (record, bytes) = sfnt.find(b"head").unwrap();

    assert_eq!(record, Record {
        tag: Tag(b"head"),
        checksum: 4165466467,
        offset: 316,
        length: 54,
    });

    assert_eq!(bytes.len(), 54);
}
```
