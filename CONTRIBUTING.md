# Contributing to sfnt

## Tests and Benchmarks

The tests and benchmarks for `sfnt` rely on font files which are not included in the `master` branch
to keep download size low. To download these font files, either execute the `resources.sh` file or
execute the `git` command contained therein.
